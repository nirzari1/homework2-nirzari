#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include "Cell.h"
#define FIFTY 50
#define HUNDRED 100

/*
This function initiates the cell
Input: string of dna sequence, and a gene
Output: none
*/
void Cell::init(const std::string dna_sequence, const Gene glucose_receptor_gene)
{
	_glocus_receptor_gene = glucose_receptor_gene;
	_nucleus.init(dna_sequence); // Initiates nucleus and gene in cell
}
/*
This function checks if the cell can create ATP
input: none
Output: true/false(can or can't)
*/
bool Cell::get_ATP()
{
	std::string RNA_transcript = "";
	Protein* protein = NULL;
	bool canProduce = false;
	RNA_transcript = _nucleus.get_RNA_transcript(_glocus_receptor_gene); // Gets RNA transcript from nucleus
	protein = _ribosome.create_protein(RNA_transcript); // Gets the protein from ribosome
	if (protein == NULL)
	{
		std::cout << "Couldn't create protein! ERROR\n";
		_exit(1); // If protein wasn't created the code exits
	}
	else
	{
		_mitochondrion.init(); // Initates the mitochondrion
		_mitochondrion.insert_glucose_receptor(*protein); // Calls fnc insert_glucose_receptor
		_mitochondrion.set_glucose(FIFTY); // Sets the glocuse to the minimum amount to create ATP
		canProduce = _mitochondrion.produceATP(); // Checks if the cell can create ATP
		if (canProduce)
		{
			_atp_units = HUNDRED; // If it can create/produce, _atp_units is changed to 100
		}
	}
	return canProduce; // Returns if it can produce ATP or not

}