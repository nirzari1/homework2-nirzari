#include <string.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Mitochondrion.h"
#define FIFTY 50

/*
This function initiates a mitochondrion
Input: none
Output: none
*/
void Mitochondrion::init()
{
	_has_glocuse_receptor = false;
	_glocuse_level = 0; // initates fields
}
/*
This function checks if the protein is the one we need
Input: protein
Output: none
*/
void Mitochondrion::insert_glucose_receptor(const Protein& protein)
{
	AminoAcidNode* temp = protein.get_first();
	if (temp->get_data() == ALANINE)
	{
		temp = temp->get_next();
		if (temp->get_data() == LEUCINE)
		{
			temp = temp->get_next();
			if (temp->get_data() == GLYCINE)
			{
				temp = temp->get_next();
				if (temp->get_data() == HISTIDINE)
				{
					temp = temp->get_next();
					if (temp->get_data() == LEUCINE)
					{
						temp = temp->get_next();
						if (temp->get_data() == PHENYLALANINE)
						{
							temp = temp->get_next();
							if (temp->get_data() == AMINO_CHAIN_END)
							{
								_has_glocuse_receptor = true;
								// We need ALANINE->LEUCINE->GLYCINE->HISTIDINE->LEUCINE->PHENYLALANINE->AMINO_CHAIN_END
								// It goes 1 by 1 and checks if thats the exact order
							}
						}
					}
				}
			}
		}
	}
}
/*
This function sets glocuse level
Input: new glocus level
Output: none
*/
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units; // sets new glocuse level
}
/*
This function checks if the cell can produce ATP
Input: none
Output: true/false(can produce or can't)
*/
bool Mitochondrion::produceATP() const
{
	bool canProduce = false;
	if (_has_glocuse_receptor == true && _glocuse_level >= FIFTY) // If has_glocuse_recetpr is true and the glocuse level is above or equal to 50
	{
		canProduce = true; // Can produce ATP
	}
	return canProduce; // Returns true/false
}