﻿#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Nucleus.h"

/*
This function initiates a gene
Input: start index of gene, end index of gene, is  the gene on the dna strand of complementray dna strand
Output: none
*/
void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand)
{
	_start = start;
	_end = end;
	_on_complementary_dna_strand = on_complementary_dna_strand;
	// Puts all info inside gene
}
// Next 3 functions are getters for Gene (getters for start, end, and on_complementary_dna_strand)
unsigned int Gene::get_start() const
{
	return _start;
}
unsigned int Gene::get_end() const
{
	return _end;
}
bool Gene::is_on_complementary_dna_strand() const
{
	return _on_complementary_dna_strand;
}
// Next 3 functions are setters for Gene (setters for start, end, and on_complementary_dna_strand)
void Gene::setStart(const unsigned int start)
{
	_start = start;
}
void Gene::setEnd(const unsigned int end)
{
	_end = end;
}
void Gene::set_on_complementary_dna_strand(const bool on_complementary_dna_strand)
{
	_on_complementary_dna_strand = on_complementary_dna_strand;
}
/*
This function initiates a nucleus
Input: A dna sequence
Output: none
*/
void Nucleus::init(const std::string dna_sequence)
{
	_DNA_strand = dna_sequence; // Saves the dna sequence
	int i = 0;
	int noWrongLetters = 0;
	int dnaLength = 0;
	std::string new_dna = "";
	dnaLength = dna_sequence.size();
	for (i = 0; i < dnaLength; i++)
	{
		if (dna_sequence[i] != 'A' && dna_sequence[i] != 'G' && dna_sequence[i] != 'C' && dna_sequence[i] != 'T')
		{
			std::cerr << "Contains other letters than A , G , C , T\n";
			_exit(1);
			// If the dna sequence contains letters that arent a, g, c, t, the program shuts down.
		}
		else
		{
			if (dna_sequence[i] == 'A')
			{
				new_dna += 'T';
			}
			else if (dna_sequence[i] == 'G')
			{
				new_dna += 'C';
			}
			else if (dna_sequence[i] == 'C')
			{
				new_dna += 'G';
			}
			else if (dna_sequence[i] == 'T')
			{
				new_dna += 'A';
			}
			// Creates the complementary dna strand (a -> <- t, g -> <- c)
		}
	}
	_complementary_DNA_strand = new_dna;
}
/*
This function is used to get the RNA transcript
Input: A Gene (all of its info)
Output: RNA transcript
*/
std::string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	int start = 0;
	int end = 0;
	int i = 0;
	std::string RNA_transcript = "";
	end = gene.get_end();
	start = gene.get_start(); // Gets starting and ending indexes of gene
	if (gene.is_on_complementary_dna_strand() == true) // Checks if the gene is on dna strand or complementary dna strand
	{
		for (i = start; i <= end; i++)
		{
			if (_complementary_DNA_strand[i] != 'T')
			{
				RNA_transcript += _complementary_DNA_strand[i];
			}
			else
			{
				RNA_transcript += 'U';
			}
			// Gets the RNA transcript from the complementary dna strand, If there's a T its swapped with U.
		}
	}
	else // If its on dna strand (not on complementary dna strand)
	{
		for (i = start; i <= end; i++)
		{
			if (_DNA_strand[i] != 'T')
			{
				RNA_transcript += _DNA_strand[i];
			}
			else
			{
				RNA_transcript += 'U';
			}
			// Gets the RNA transcript from the dna strand, If there's a T its swapped with U
		}

	}
	return RNA_transcript;
}
/*
This function reverses the DNA strand
Input: none
Output: reversed DNA strand
*/
std::string Nucleus::get_reversed_DNA_strand() const
{
	int dnaLength = 0;
	int i = 0;
	std::string reversed_DNA = "";
	dnaLength = _DNA_strand.length(); // Gets length of the dna strand
	for (i = dnaLength - 1; i >= 0; i--)
	{
		reversed_DNA += _DNA_strand[i]; // Goes from the last index and adds all of the chars from the ending to a new string
	}
	return reversed_DNA;
}
/*
This function gets number of codon appereances
Input: a codon
Output: amount of times the codon appears in the dna strand
*/
unsigned int Nucleus::get_num_of_codon_appearances(const std::string& codon) const
{
	unsigned int count = 0; //sets count
	size_t counter = 0; //sets counter
	while (counter != std::string::npos) // if counter does not equal string no position
	{
		size_t i = counter + codon.length(); // sets i to counter + length of searched for object
		counter = _DNA_strand.find(codon, i);
		count++;
	}
	return count;
}
