#ifndef RIBOSOME_H
#define RIBOSOME_H
#include "Protein.h"
#include "Nucleus.h"
class Ribosome
{
public:
	// method
	Protein* create_protein(std::string& RNA_transcript) const;
};
#endif