#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Ribosome.h"
#define THREE 3
/*
This function creates protein
Input: the RNA transcript
Output: protein
*/
Protein* Ribosome::create_protein(std::string& RNA_transcript) const
{
	Protein* newProtein = new Protein;
	int RNA_transcript_len = 0;
	int i = 0;
	std::string threeNoc = "";
	std::string& b = threeNoc;
	newProtein->init(); // Initiates the protein
	RNA_transcript_len = RNA_transcript.size(); // Gets length of RNA transcript
	while (RNA_transcript_len >= THREE) // While there are 3 or more chars in RNA_transcript
	{
		for (i = 0; i < THREE; i++)
		{
			threeNoc += RNA_transcript[i]; // Gets 3 first chars from RNA_transcript and puts it into threeNoc
		}
		RNA_transcript = RNA_transcript.substr(THREE); // Removes first 3 chars from RNA_transcript
		if ((get_amino_acid(b)) != UNKNOWN) // If the amino acid isnt unknown
		{
			newProtein->add(get_amino_acid(b)); // Adds the amino acid to the protein

		}
		else
		{
			newProtein->clear(); // If it is unknown, all the data will be removed.
		}
		threeNoc = "";
		RNA_transcript_len = RNA_transcript.size(); // Resets value before it goes into a new loop
	}
	return newProtein; // Returns the new protein
}