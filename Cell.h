#ifndef CELL_H
#define CELL_H
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Ribosome.h"
class Cell
{
public:
	// methods
	void init(const std::string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
private:
	// fields
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	Gene _glocus_receptor_gene;
	unsigned int _atp_units;
};
#endif